﻿namespace Multimedia1
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.buttonGreen = new System.Windows.Forms.Button();
            this.buttonRotate = new System.Windows.Forms.Button();
            this.buttonSqueeze = new System.Windows.Forms.Button();
            this.buttonGrid = new System.Windows.Forms.Button();
            this.buttonReflectX = new System.Windows.Forms.Button();
            this.buttonReflectY = new System.Windows.Forms.Button();
            this.buttonNegativ = new System.Windows.Forms.Button();
            this.buttonStretch = new System.Windows.Forms.Button();
            this.buttonRed = new System.Windows.Forms.Button();
            this.buttonBlue = new System.Windows.Forms.Button();
            this.buttonGray = new System.Windows.Forms.Button();
            this.buttonBarChart = new System.Windows.Forms.Button();
            this.buttonCircuit = new System.Windows.Forms.Button();
            this.buttonNoise = new System.Windows.Forms.Button();
            this.buttonScal = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(13, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(546, 397);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // buttonPrint
            // 
            this.buttonPrint.Location = new System.Drawing.Point(566, 10);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(75, 23);
            this.buttonPrint.TabIndex = 1;
            this.buttonPrint.Text = "Print";
            this.buttonPrint.UseVisualStyleBackColor = true;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // buttonGreen
            // 
            this.buttonGreen.Location = new System.Drawing.Point(647, 186);
            this.buttonGreen.Name = "buttonGreen";
            this.buttonGreen.Size = new System.Drawing.Size(75, 23);
            this.buttonGreen.TabIndex = 2;
            this.buttonGreen.Text = "Green";
            this.buttonGreen.UseVisualStyleBackColor = true;
            this.buttonGreen.Click += new System.EventHandler(this.buttonGreen_Click);
            // 
            // buttonRotate
            // 
            this.buttonRotate.Location = new System.Drawing.Point(566, 156);
            this.buttonRotate.Name = "buttonRotate";
            this.buttonRotate.Size = new System.Drawing.Size(75, 23);
            this.buttonRotate.TabIndex = 3;
            this.buttonRotate.Text = "Rotate";
            this.buttonRotate.UseVisualStyleBackColor = true;
            this.buttonRotate.Click += new System.EventHandler(this.buttonRotate_Click);
            // 
            // buttonSqueeze
            // 
            this.buttonSqueeze.Location = new System.Drawing.Point(647, 98);
            this.buttonSqueeze.Name = "buttonSqueeze";
            this.buttonSqueeze.Size = new System.Drawing.Size(75, 23);
            this.buttonSqueeze.TabIndex = 4;
            this.buttonSqueeze.Text = "Compression";
            this.buttonSqueeze.UseVisualStyleBackColor = true;
            this.buttonSqueeze.Click += new System.EventHandler(this.buttonSqueeze_Click);
            // 
            // buttonGrid
            // 
            this.buttonGrid.Location = new System.Drawing.Point(566, 39);
            this.buttonGrid.Name = "buttonGrid";
            this.buttonGrid.Size = new System.Drawing.Size(75, 23);
            this.buttonGrid.TabIndex = 5;
            this.buttonGrid.Text = "Grid";
            this.buttonGrid.UseVisualStyleBackColor = true;
            this.buttonGrid.Click += new System.EventHandler(this.buttonGrid_Click);
            // 
            // buttonReflectX
            // 
            this.buttonReflectX.Location = new System.Drawing.Point(566, 69);
            this.buttonReflectX.Name = "buttonReflectX";
            this.buttonReflectX.Size = new System.Drawing.Size(75, 23);
            this.buttonReflectX.TabIndex = 6;
            this.buttonReflectX.Text = "Reflect X";
            this.buttonReflectX.UseVisualStyleBackColor = true;
            this.buttonReflectX.Click += new System.EventHandler(this.buttonReflectX_Click);
            // 
            // buttonReflectY
            // 
            this.buttonReflectY.Location = new System.Drawing.Point(647, 69);
            this.buttonReflectY.Name = "buttonReflectY";
            this.buttonReflectY.Size = new System.Drawing.Size(75, 23);
            this.buttonReflectY.TabIndex = 7;
            this.buttonReflectY.Text = "Reflect Y";
            this.buttonReflectY.UseVisualStyleBackColor = true;
            this.buttonReflectY.Click += new System.EventHandler(this.buttonReflectY_Click);
            // 
            // buttonNegativ
            // 
            this.buttonNegativ.Location = new System.Drawing.Point(566, 127);
            this.buttonNegativ.Name = "buttonNegativ";
            this.buttonNegativ.Size = new System.Drawing.Size(75, 23);
            this.buttonNegativ.TabIndex = 8;
            this.buttonNegativ.Text = "Negativ";
            this.buttonNegativ.UseVisualStyleBackColor = true;
            this.buttonNegativ.Click += new System.EventHandler(this.buttonNegativ_Click);
            // 
            // buttonStretch
            // 
            this.buttonStretch.Location = new System.Drawing.Point(566, 98);
            this.buttonStretch.Name = "buttonStretch";
            this.buttonStretch.Size = new System.Drawing.Size(75, 23);
            this.buttonStretch.TabIndex = 9;
            this.buttonStretch.Text = "Stretch";
            this.buttonStretch.UseVisualStyleBackColor = true;
            this.buttonStretch.Click += new System.EventHandler(this.buttonStretch_Click);
            // 
            // buttonRed
            // 
            this.buttonRed.Location = new System.Drawing.Point(566, 186);
            this.buttonRed.Name = "buttonRed";
            this.buttonRed.Size = new System.Drawing.Size(75, 23);
            this.buttonRed.TabIndex = 10;
            this.buttonRed.Text = "Red";
            this.buttonRed.UseVisualStyleBackColor = true;
            this.buttonRed.Click += new System.EventHandler(this.buttonRed_Click);
            // 
            // buttonBlue
            // 
            this.buttonBlue.Location = new System.Drawing.Point(566, 216);
            this.buttonBlue.Name = "buttonBlue";
            this.buttonBlue.Size = new System.Drawing.Size(75, 23);
            this.buttonBlue.TabIndex = 11;
            this.buttonBlue.Text = "Blue";
            this.buttonBlue.UseVisualStyleBackColor = true;
            this.buttonBlue.Click += new System.EventHandler(this.buttonBlue_Click);
            // 
            // buttonGray
            // 
            this.buttonGray.Location = new System.Drawing.Point(647, 216);
            this.buttonGray.Name = "buttonGray";
            this.buttonGray.Size = new System.Drawing.Size(75, 23);
            this.buttonGray.TabIndex = 12;
            this.buttonGray.Text = "Gray";
            this.buttonGray.UseVisualStyleBackColor = true;
            this.buttonGray.Click += new System.EventHandler(this.buttonGray_Click);
            // 
            // buttonBarChart
            // 
            this.buttonBarChart.Location = new System.Drawing.Point(566, 246);
            this.buttonBarChart.Name = "buttonBarChart";
            this.buttonBarChart.Size = new System.Drawing.Size(75, 23);
            this.buttonBarChart.TabIndex = 13;
            this.buttonBarChart.Text = "Bar chart";
            this.buttonBarChart.UseVisualStyleBackColor = true;
            this.buttonBarChart.Click += new System.EventHandler(this.buttonBarChart_Click);
            // 
            // buttonCircuit
            // 
            this.buttonCircuit.Location = new System.Drawing.Point(566, 276);
            this.buttonCircuit.Name = "buttonCircuit";
            this.buttonCircuit.Size = new System.Drawing.Size(75, 23);
            this.buttonCircuit.TabIndex = 14;
            this.buttonCircuit.Text = "Circuit";
            this.buttonCircuit.UseVisualStyleBackColor = true;
            this.buttonCircuit.Click += new System.EventHandler(this.buttonCircuit_Click);
            // 
            // buttonNoise
            // 
            this.buttonNoise.Location = new System.Drawing.Point(566, 306);
            this.buttonNoise.Name = "buttonNoise";
            this.buttonNoise.Size = new System.Drawing.Size(75, 23);
            this.buttonNoise.TabIndex = 15;
            this.buttonNoise.Text = "Noise";
            this.buttonNoise.UseVisualStyleBackColor = true;
            this.buttonNoise.Click += new System.EventHandler(this.buttonNoise_Click);
            // 
            // buttonScal
            // 
            this.buttonScal.Location = new System.Drawing.Point(566, 335);
            this.buttonScal.Name = "buttonScal";
            this.buttonScal.Size = new System.Drawing.Size(75, 23);
            this.buttonScal.TabIndex = 16;
            this.buttonScal.Text = "Scaling";
            this.buttonScal.UseVisualStyleBackColor = true;
            this.buttonScal.Click += new System.EventHandler(this.buttonScal_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 422);
            this.Controls.Add(this.buttonScal);
            this.Controls.Add(this.buttonNoise);
            this.Controls.Add(this.buttonCircuit);
            this.Controls.Add(this.buttonBarChart);
            this.Controls.Add(this.buttonGray);
            this.Controls.Add(this.buttonBlue);
            this.Controls.Add(this.buttonRed);
            this.Controls.Add(this.buttonStretch);
            this.Controls.Add(this.buttonNegativ);
            this.Controls.Add(this.buttonReflectY);
            this.Controls.Add(this.buttonReflectX);
            this.Controls.Add(this.buttonGrid);
            this.Controls.Add(this.buttonSqueeze);
            this.Controls.Add(this.buttonRotate);
            this.Controls.Add(this.buttonGreen);
            this.Controls.Add(this.buttonPrint);
            this.Controls.Add(this.pictureBox1);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonPrint;
        private System.Windows.Forms.Button buttonGreen;
        private System.Windows.Forms.Button buttonRotate;
        private System.Windows.Forms.Button buttonSqueeze;
        private System.Windows.Forms.Button buttonGrid;
        private System.Windows.Forms.Button buttonReflectX;
        private System.Windows.Forms.Button buttonReflectY;
        private System.Windows.Forms.Button buttonNegativ;
        private System.Windows.Forms.Button buttonStretch;
        private System.Windows.Forms.Button buttonRed;
        private System.Windows.Forms.Button buttonBlue;
        private System.Windows.Forms.Button buttonGray;
        private System.Windows.Forms.Button buttonBarChart;
        private System.Windows.Forms.Button buttonCircuit;
        private System.Windows.Forms.Button buttonNoise;
        private System.Windows.Forms.Button buttonScal;
    }
}

