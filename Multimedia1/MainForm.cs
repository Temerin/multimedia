﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimpleSignal;
using ZedGraph;


namespace Multimedia1
{
    public partial class MainForm : Form
    {
        static string path = @"D:\test.bmp";
        public MainForm()
        {
            InitializeComponent();

        }

        /* public int[] redP = new int[256];
         public int[] greenP = new int[256];
         public int[] blueP = new int[256];*/
        Bitmap image1 = new Bitmap(path);

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            image1 = new Bitmap(path);
            pictureBox1.Image = image1;
        }

        private void buttonGreen_Click(object sender, EventArgs e)
        {
            int x, y;
            for (x = 0; x < image1.Width; x++)
            {
                for (y = 0; y < image1.Height; y++)
                {
                    Color pixelColor = image1.GetPixel(x, y);
                    Color newColor = Color.FromArgb(0, pixelColor.G, 0);
                    image1.SetPixel(x, y, newColor);
                }
            }
            pictureBox1.Image = image1;
        }

        private void buttonRotate_Click(object sender, EventArgs e)
        {
            RotateLeft();
            //ReflectY();
            pictureBox1.Image = image1;
        }

        public void ReflectX()
        {
            Bitmap image2 = new Bitmap(image1.Width, image1.Height);
            int x, y, newX;
            for (x = 0; x < image1.Width; x++)
            {
                for (y = 0; y < image1.Height; y++)
                {
                    Color pixelColor = image1.GetPixel(x, y);
                    newX = image2.Width - x - 1;
                    image2.SetPixel(newX, y, pixelColor);
                }
            }
            image1 = image2;
        }

        public void ReflectY()
        {
            Bitmap image2 = new Bitmap(image1.Width, image1.Height);
            int x, y, newY;
            for (x = 0; x < image1.Width; x++)
            {
                for (y = 0; y < image1.Height; y++)
                {
                    Color pixelColor = image1.GetPixel(x, y);
                    newY = image2.Height - y - 1;
                    image2.SetPixel(x, newY, pixelColor);
                }
            }
            image1 = image2;
        }

        // public void RotateRight
        public void RotateLeft()
        {
            Bitmap image2 = new Bitmap(image1.Height, image1.Width);
            int x, y, newY;
            for (y = 0; y < image1.Height; y++)
            {
                for (x = 0; x < image1.Width; x++)
                {
                    Color pixelColor = image1.GetPixel(x, y);
                    newY = image2.Height - x - 1;
                    image2.SetPixel(y, newY, pixelColor);
                }
            }
            image1 = image2;
        }

        public void SqueezeX()
        {
            Bitmap image2 = new Bitmap(image1.Width / 2, image1.Height);
            int x, y;
            for (y = 0; y < image1.Height; y++)
            {
                for (x = 0; x < image1.Width / 2; x++)
                {
                    Color pixelColor1 = image1.GetPixel((x * 2) + 1, y);
                    Color pixelColor2 = image1.GetPixel(x * 2, y);
                    Color pixelColor = Color.FromArgb((pixelColor1.R + pixelColor1.R) / 2, (pixelColor1.G + pixelColor1.G) / 2, (pixelColor1.B + pixelColor1.B) / 2);
                    image2.SetPixel(x, y, pixelColor);
                }
            }
            image1 = image2;
        }

        public void SqueezeY()
        {
            Bitmap image2 = new Bitmap(image1.Width, image1.Height / 2);
            int x, y;
            for (y = 0; y < image1.Height / 2; y++)
            {
                for (x = 0; x < image1.Width; x++)
                {
                    Color pixelColor1 = image1.GetPixel(x, (y * 2) + 1);
                    Color pixelColor2 = image1.GetPixel(x, y * 2);
                    Color pixelColor = Color.FromArgb((pixelColor1.R + pixelColor1.R) / 2, (pixelColor1.G + pixelColor1.G) / 2, (pixelColor1.B + pixelColor1.B) / 2);
                    image2.SetPixel(x, y, pixelColor);
                }
            }
            image1 = image2;
        }

        private void buttonSqueeze_Click(object sender, EventArgs e)
        {
            SqueezeX();
            SqueezeY();
            pictureBox1.Image = image1;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void buttonGrid_Click(object sender, EventArgs e)
        {
            Bitmap image2 = new Bitmap(image1.Width, image1.Height);
            int x, y, xG, yG;
            xG = 0;
            yG = 0;
            Color pixelColor;
            for (y = 0; y < image1.Height; y++)
            {
                if (yG++ > 50)
                {
                    pixelColor = Color.FromArgb(255, 255, 255);
                }
                else
                {
                    xG = 0;
                    for (x = 0; x < image1.Width; x++)
                    {
                        if (xG++ > 50)
                        {
                            pixelColor = Color.FromArgb(255, 255, 255);
                        }
                        else
                        {
                            pixelColor = image1.GetPixel(x, y);
                        }
                        if (xG > 51) xG = 0;
                        image2.SetPixel(x, y, pixelColor);
                    }
                }
                if (yG > 51) yG = 0;
            }
            image1 = image2;
            pictureBox1.Image = image1;
        }

        private void buttonReflectX_Click(object sender, EventArgs e)
        {
            ReflectX();
            pictureBox1.Image = image1;
        }

        private void buttonReflectY_Click(object sender, EventArgs e)
        {
            ReflectY();
            pictureBox1.Image = image1;
        }

        public void StretchX()
        {
            Bitmap image2 = new Bitmap(image1.Width * 2, image1.Height);
            int x, xS, y;
            for (y = 0; y < image1.Height; y++)
            {
                xS = 0;
                for (x = 0; x < image1.Width; x++)
                {
                    Color pixelColor = image1.GetPixel(x, y);
                    image2.SetPixel(xS++, y, pixelColor);
                    image2.SetPixel(xS++, y, pixelColor);
                }
            }
            image1 = image2;
        }

        public void StretchY()
        {
            Bitmap image2 = new Bitmap(image1.Width, image1.Height * 2);
            int x, yS, y;
            for (x = 0; x < image1.Width; x++)
            {
                yS = 0;
                for (y = 0; y < image1.Height; y++)
                {
                    Color pixelColor = image1.GetPixel(x, y);
                    image2.SetPixel(x, yS++, pixelColor);
                    image2.SetPixel(x, yS++, pixelColor);
                }
            }
            image1 = image2;
        }

        private void buttonStretch_Click(object sender, EventArgs e)
        {
            StretchX();
            StretchY();
            pictureBox1.Image = image1;
        }

        private void buttonNegativ_Click(object sender, EventArgs e)
        {
            int x, y;
            for (x = 0; x < image1.Width; x++)
            {
                for (y = 0; y < image1.Height; y++)
                {
                    Color pixelColor = image1.GetPixel(x, y);
                    Color newColor = Color.FromArgb((255 - pixelColor.R), (255 - pixelColor.G), (255 - pixelColor.B));
                    image1.SetPixel(x, y, newColor);
                }
            }
            pictureBox1.Image = image1;
        }

        private void buttonCircuit_Click(object sender, EventArgs e)
        {
            Bitmap image2 = new Bitmap(image1.Width, image1.Height);
            int x, y;
            int r, g, b;
            for (y = 1; y < image1.Height - 1; y++)
            {
                for (x = 1; x < image1.Width - 1; x++)
                {
                    Color pixelColorС = image1.GetPixel(x, y);
                    Color pixelColorT = image1.GetPixel(x + 1, y + 1);
                    /*Color pixelColorB = image1.GetPixel(x, y - 1);
                    Color pixelColorL = image1.GetPixel(x - 1, y);
                    Color pixelColorR = image1.GetPixel(x + 1, y);
                    Color pixelColorTL = image1.GetPixel(x - 1, y + 1);
                    Color pixelColorTR = image1.GetPixel(x + 1, y + 1);
                    Color pixelColorBL = image1.GetPixel(x - 1, y - 1);
                    Color pixelColorBR = image1.GetPixel(x + 1, y - 1);*/
                    r = Convert.ToInt32(1 * (pixelColorС.R - pixelColorT.R));
                    if (r < 0) r = 0;
                    if (r > 255) r = 255;
                    g = Convert.ToInt32(1 * (pixelColorС.G - pixelColorT.G));
                    if (g < 0) g = 0;
                    if (g > 255) g = 255;
                    b = Convert.ToInt32(1 * (pixelColorС.B - pixelColorT.B));
                    if (b < 0) b = 0;
                    if (b > 255) b = 255;
                    Color newColor = Color.FromArgb(r, g, b);
                    image2.SetPixel(x, y, newColor);
                }
            }
            image1 = image2;
            pictureBox1.Image = image1;
        }

        private void buttonRed_Click(object sender, EventArgs e)
        {
            int x, y;
            for (x = 0; x < image1.Width; x++)
            {
                for (y = 0; y < image1.Height; y++)
                {
                    Color pixelColor = image1.GetPixel(x, y);
                    Color newColor = Color.FromArgb(pixelColor.R, 0, 0);
                    image1.SetPixel(x, y, newColor);
                }
            }
            pictureBox1.Image = image1;
        }

        private void buttonBlue_Click(object sender, EventArgs e)
        {
            int x, y;
            for (x = 0; x < image1.Width; x++)
            {
                for (y = 0; y < image1.Height; y++)
                {
                    Color pixelColor = image1.GetPixel(x, y);
                    Color newColor = Color.FromArgb(0, 0, pixelColor.B);
                    image1.SetPixel(x, y, newColor);
                }
            }
            pictureBox1.Image = image1;
        }

        private void buttonGray_Click(object sender, EventArgs e)
        {
            int x, y, rgb;
            for (x = 0; x < image1.Width; x++)
            {
                for (y = 0; y < image1.Height; y++)
                {
                    Color pixelColor = image1.GetPixel(x, y);
                    rgb = (pixelColor.R + pixelColor.G + pixelColor.B) / 3;
                    Color newColor = Color.FromArgb(rgb, rgb, rgb);
                    image1.SetPixel(x, y, newColor);
                }
            }
            pictureBox1.Image = image1;
        }

        private void buttonBarChart_Click(object sender, EventArgs e)
        {
            int[] redP = new int[256];
            int[] greenP = new int[256];
            int[] blueP = new int[256];
            int x, y;
            for (int i = 0; i < 256; i++)
            {
                redP[i] = 0;
                greenP[i] = 0;
                blueP[i] = 0;
            }
            for (x = 0; x < image1.Width; x++)
            {
                for (y = 0; y < image1.Height; y++)
                {
                    Color pixelColor = image1.GetPixel(x, y);
                    redP[pixelColor.R]++;
                    greenP[pixelColor.G]++;
                    blueP[pixelColor.B]++;
                }
            }
            SimpleSignal.MainForm f2 = new SimpleSignal.MainForm(redP, greenP, blueP);
            f2.Show();
        }

        private void buttonNoise_Click(object sender, EventArgs e)
        {
            int x, y;
            int[,] rgb = new int[image1.Width, image1.Height];
            for (x = 0; x < image1.Width; x++)
            {
                for (y = 0; y < image1.Height; y++)
                {
                    Color pixelColor = image1.GetPixel(x, y);
                    rgb[x, y] = (pixelColor.R + pixelColor.G + pixelColor.B) / 3;
                }
            }
            NoiseSuppression f2 = new NoiseSuppression(rgb, image1.Width, image1.Height);
            f2.Show();
            pictureBox1.Image = image1;
        }

        private void buttonScal_Click(object sender, EventArgs e)
        {
            int x, y;
            int[,] rgb = new int[image1.Width, image1.Height];
            for (x = 0; x < image1.Width; x++)
            {
                for (y = 0; y < image1.Height; y++)
                {
                    Color pixelColor = image1.GetPixel(x, y);
                    rgb[x, y] = (pixelColor.R + pixelColor.G + pixelColor.B) / 3;
                }
            }
            Scaling f2 = new Scaling(rgb, image1.Width, image1.Height);
            f2.Show();
            pictureBox1.Image = image1;
        }
    }
}
