﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Multimedia1
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());

            //Bitmap image1 = new Bitmap(@"G:\Multimedia1\Multimedia1\test.bmp");//"твой путь до файла");
           // Color[][] imageArray = GetImageArray(image1);
        }

        /*public static Color[][] GetImageArray(Bitmap srcImage)
        {
            if (srcImage == null)
                throw new ArgumentNullException("srcImage");

            Color[][] result = new Color[srcImage.Height][];
            for (var y = 0; y < srcImage.Height; y++)
            {
                result[y] = new Color[srcImage.Width];
                for (var x = 0; x < srcImage.Width; x++)
                {
                    Color srcPixel = srcImage.GetPixel(x, y);
                    result[y][x] = srcPixel;
                }
            }

            return result;
        }*/
    }
}
