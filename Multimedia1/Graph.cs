﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Multimedia1;
using ZedGraph;

namespace SimpleSignal
{
	public partial class MainForm : Form
	{
        /*  private int[] RedP; //= new int[256];

          public int[] RedPix(int[] R;)
          {
              RedP=R;
          }
          public int[] GreenP = new int[256];
          public int[] BlueP = new int[256];*/
         
       // Bitmap image1 = new Bitmap(@"D:\test.bmp");
        int[] redP = new int[256];
        int[] greenP = new int[256];
        int[] blueP = new int[256];
         
        public MainForm (int[] a, int [] b, int[] c)
		{
			InitializeComponent ();
            redP = a;
            greenP = b;
            blueP = c;

            DrawGraph();
        }


		private void DrawGraph ()
		{
            
			// Получим панель для рисования
			GraphPane pane = zedGraph.GraphPane;

			// Очистим список кривых на тот случай, если до этого сигналы уже были нарисованы
			pane.CurveList.Clear ();

			// Создадим список точек
			PointPairList listR = new PointPairList ();
            PointPairList listG = new PointPairList ();
            PointPairList listB = new PointPairList ();


			// Заполняем список точек
			for (int x = 0; x < 255; x++)
			{
				// добавим в список точку
				listR.Add (x, redP[x]);
                listG.Add(x, greenP[x]);
                listB.Add(x, blueP[x]);

            }

			// Создадим кривую с названием "Sinc", 
			// которая будет рисоваться голубым цветом (Color.Blue),
			// Опорные точки выделяться не будут (SymbolType.None)
			LineItem myCurve = pane.AddCurve ("Red", listR, Color.Red, SymbolType.None);
            LineItem myCurve1 = pane.AddCurve("Green", listG, Color.Green, SymbolType.None);
            LineItem myCurve2 = pane.AddCurve("Blue", listB, Color.Blue, SymbolType.None);

            // Вызываем метод AxisChange (), чтобы обновить данные об осях. 
            // В противном случае на рисунке будет показана только часть графика, 
            // которая умещается в интервалы по осям, установленные по умолчанию
            zedGraph.AxisChange ();

			// Обновляем график
			zedGraph.Invalidate ();
		}
	}
}