﻿namespace Multimedia1
{
    partial class Scaling
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonMedian = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonBilin = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonMedian
            // 
            this.buttonMedian.Location = new System.Drawing.Point(1772, 58);
            this.buttonMedian.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonMedian.Name = "buttonMedian";
            this.buttonMedian.Size = new System.Drawing.Size(147, 35);
            this.buttonMedian.TabIndex = 6;
            this.buttonMedian.Text = "Бикубическое";
            this.buttonMedian.UseVisualStyleBackColor = true;
            this.buttonMedian.Click += new System.EventHandler(this.buttonMedian_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(18, 18);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1731, 985);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // buttonBilin
            // 
            this.buttonBilin.Location = new System.Drawing.Point(1772, 14);
            this.buttonBilin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonBilin.Name = "buttonBilin";
            this.buttonBilin.Size = new System.Drawing.Size(147, 35);
            this.buttonBilin.TabIndex = 4;
            this.buttonBilin.Text = "Билинейное";
            this.buttonBilin.UseVisualStyleBackColor = true;
            this.buttonBilin.Click += new System.EventHandler(this.buttonBilin_Click);
            // 
            // Scaling
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1916, 1017);
            this.Controls.Add(this.buttonMedian);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buttonBilin);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Scaling";
            this.Text = "Scaling";
            this.Load += new System.EventHandler(this.Scaling_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonMedian;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonBilin;
    }
}