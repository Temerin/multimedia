﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Math;
using System.Net.Mime;

namespace Multimedia1
{
    public partial class NoiseSuppression : Form
    {
        int[,] rgb;
        int w, h;
        public NoiseSuppression()
        {
            InitializeComponent();
        }

        public int Median(int a, int b, int c)
        {
            double m;
            a *= a;
            b *= b;
            c *= c;
            m = Math.Sqrt((2*b) + (2*c) - a);
            m /= 2;
            return Convert.ToInt32(m);
        }
        public NoiseSuppression(int[,] redGreenBlue, int width, int height)
        {
            InitializeComponent();
            w = width;
            h = height;
            rgb = new int[w, h];
            rgb = redGreenBlue;
        }

        private void buttonMedian_Click(object sender, EventArgs e)
        {
            Bitmap image = new Bitmap(w - 2, h - 2);

            int x, y;
            int r, g, b;
            int neig = 1;
            int m1, m2, m3, m4, mA, mB;
            int[,] rgb2 = new int[w, h];
            for (y = neig; y < h - neig - 1; y++)
            {
                for (x = neig; x < w - neig; x++)
                {
                    rgb2[x, y] = 0;
                    int[] median = new int[] {  rgb[x - 1, y - 1],  rgb[x, y - 1],  rgb[x, y - 1],
                                                rgb[x - 1, y],      rgb[x, y],      rgb[x, y - 1],
                                                rgb[x - 1, y + 1],  rgb[x, y + 1],  rgb[x, y + 1]
                                             };
                    Array.Sort(median);
                    Color newColor = Color.FromArgb(median[5], median[5], median[5]);
                    image.SetPixel(x - 1, y - 1, newColor);
                }
            }
            //осортировать пиксели по возрастанию. Взять средний из них. Если пикселей чётное количество, то ср. арифм. из средних.
            pictureBox1.Image = image;
        }

        private void buttonGauss_Click(object sender, EventArgs e)
        {
            Bitmap image = new Bitmap(w - 2, h - 2);
            int x, y, x2, y2;
            int r, g, b;
            int neig = 2;
            int[,] rgb2 = new int[w, h];
            for (y = neig; y < h - neig; y++)
            {
                for (x = neig; x < w - neig; x++)
                {
                    rgb2[x, y] = 0;
                    rgb2[x, y] += rgb[x-2, y-2] + rgb[x + 2, y - 2] + rgb[x - 2, y + 2] + rgb[x + 2, y + 2]; // угловые (x1)
                    rgb2[x, y] += (rgb[x - 1, y - 2] + rgb[x + 1, y - 2] + rgb[x - 2, y - 1] + rgb[x + 2, y - 1]
                        + rgb[x - 2, y + 1] + rgb[x + 2, y + 1] + rgb[x - 1, y + 2] + rgb[x + 1, y + 2]) * 4; // x4
                    rgb2[x, y] += (rgb[x - 2, y - 2] + rgb[x - 2, y] + rgb[x + 2, y] + rgb[x + 2, y + 2]) * 7; // x7
                    rgb2[x, y] += (rgb[x - 1, y - 1] + rgb[x + 1, y - 1] + rgb[x - 1, y + 1] + rgb[x + 1, y + 1]) * 16; // x16
                    rgb2[x, y] += (rgb[x, y - 1] + rgb[x - 1, y] + rgb[x + 1, y] + rgb[x, y + 1]) * 26; // x26
                    rgb2[x, y] += rgb[x, y] * 41;
                    rgb2[x, y] /= 273; 
                    if (rgb2[x, y] < 0) rgb2[x, y] = 0;
                    if (rgb2[x, y] > 255) rgb2[x, y] = 255;
                    Color newColor = Color.FromArgb(rgb2[x, y], rgb2[x, y], rgb2[x, y]);
                    image.SetPixel(x - 1, y - 1, newColor);
                }
            }
            pictureBox1.Image = image;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap image = new Bitmap(w-2, h-2);
            int x, y, x2, y2;
            int r, g, b;
            int neig = 1;
            int[,] rgb2 = new int[w,h];
            for (y = neig; y < h - neig-1; y++)
            {
                for (x = neig; x < w - neig; x++)
                {
                    rgb2[x, y] = 0;
                    for (x2 = x - neig; x2 <= (x + neig); x2++)
                    {
                        for (y2 = y - neig; y2 <= (y + neig); y2++)
                        {
                            rgb2[x, y] += rgb[x2, y2];
                        }
                    }
                    rgb2[x, y] /= 9; // !!!!!!Переделать на зависимость от neig!!!!! 
                    if (rgb2[x, y] < 0) rgb2[x, y] = 0;
                    if (rgb2[x, y] > 255) rgb2[x, y] = 255;
                    Color newColor = Color.FromArgb(rgb2[x, y], rgb2[x, y], rgb2[x, y]);
                    image.SetPixel(x-1,y-1, newColor);
                }
            }
            pictureBox1.Image = image;
        }
    }
}
