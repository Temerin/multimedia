﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Multimedia1
{
    public partial class Scaling : Form
    {
        int[,] rgb;
        int w, h;
        public Scaling(int[,] redGreenBlue, int width, int height)
        {
            InitializeComponent();
            w = width;
            h = height;
            rgb = new int[w, h];
            rgb = redGreenBlue;
        }
        public Scaling()
        {
            InitializeComponent();
        }

        private void Scaling_Load(object sender, EventArgs e)
        {

        }

        private void buttonBilin_Click(object sender, EventArgs e)
        {
            {
                Bitmap image = new Bitmap(w * 2, h * 2);

                int x, y;
                int r, g, b;
                int neig = 1;
                int newPixel = 0;
                for (y = neig; y < h - neig - 1; y++)
                {
                    for (x = neig; x < w - neig; x++)
                    {
                        Color newColor = Color.FromArgb(rgb[x, y], rgb[x, y], rgb[x, y]);
                        image.SetPixel(x * 2 - 1, y - 1, newColor); //Не охватывается нулевой пиксель!!! присвоение старого пикселя
                        newPixel = BilinScal(x - 1, y - 1, 1, 1, true);
                        newColor = Color.FromArgb(newPixel, newPixel, newPixel);
                        image.SetPixel(x * 2, y - 1, newColor);
                    }
                }
                rgb = new int[image.Width, image.Height];
                for (x = 0; x < image.Width; x++)
                {
                    for (y = 0; y < image.Height; y++)
                    {
                        Color pixelColor = image.GetPixel(x, y);
                        rgb[x, y] = (pixelColor.R + pixelColor.G + pixelColor.B) / 3;
                    }
                }
                for (y = neig; y < h - neig - 1; y++)
                {
                    for (x = neig; x < w * 2 - neig; x++)
                    {
                        Color newColor = Color.FromArgb(rgb[x, y], rgb[x, y], rgb[x, y]);
                        image.SetPixel(x - 1, y * 2 - 1, newColor); //Не охватывается нулевой пиксель!!! присвоение старого пикселя
                        newPixel = BilinScal(x - 1, y - 1, 1, 1, false);
                        newColor = Color.FromArgb(newPixel, newPixel, newPixel);
                        image.SetPixel(x - 1, y * 2, newColor);
                    }
                }
                pictureBox1.Image = image;
            }
        }

        private void buttonMedian_Click(object sender, EventArgs e)
        {
            Bitmap image = new Bitmap(w * 2, h * 2);

            int x, y;
            int r, g, b;
            int neig = 1;
            int newPixel = 0;
            for (y = neig; y < h - neig - 1; y++)
            {
                for (x = neig; x < w - neig - 1; x++)
                {
                    Color newColor = Color.FromArgb(rgb[x, y], rgb[x, y], rgb[x, y]);
                    image.SetPixel(x * 2 - 1, y - 1, newColor); //Не охватывается нулевой пиксель!!! присвоение старого пикселя
                    double[,] p = new double[4, 4] {{  rgb[x - 1, y - 1],  rgb[x, y - 1],  rgb[x + 1, y - 1], rgb[x+2, y - 1] },
                                                    {  rgb[x - 1, y],      rgb[x, y],      rgb[x + 1, y],     rgb[x+2, y]     },
                                                    {  rgb[x - 1, y + 1],  rgb[x, y + 1],  rgb[x + 1, y + 1], rgb[x+2, y + 1] },
                                                    {  rgb[x - 1, y + 2],  rgb[x, y + 2],  rgb[x + 1, y + 2], rgb[x+2, y + 2] },}; 
                    newPixel = Convert.ToInt32(bicubicInterpolate(p, 0.5, 0.25));
                    if (newPixel < 0) newPixel = 0;
                    if (newPixel > 255) newPixel = 255;
                    newColor = Color.FromArgb(newPixel, newPixel, newPixel);
                    image.SetPixel(x * 2, y - 1, newColor);
                }
            }
            rgb = new int[image.Width, image.Height];
            for (x = 0; x < image.Width; x++)
            {
                for (y = 0; y < image.Height; y++)
                {
                    Color pixelColor = image.GetPixel(x, y);
                    rgb[x, y] = (pixelColor.R + pixelColor.G + pixelColor.B) / 3;
                }
            }
            for (y = neig; y < h - neig - 1; y++)
            {
                for (x = neig; x < w * 2 - neig-1; x++)
                {
                    Color newColor = Color.FromArgb(rgb[x, y], rgb[x, y], rgb[x, y]);
                    image.SetPixel(x - 1, y * 2 - 1, newColor); //Не охватывается нулевой пиксель!!! присвоение старого пикселя
                    double[,] p = new double[4, 4] {{  rgb[x - 1, y - 1],  rgb[x, y - 1],  rgb[x + 1, y - 1], rgb[x+2, y - 1] },
                                                    {  rgb[x - 1, y],      rgb[x, y],      rgb[x + 1, y],     rgb[x+2, y]     },
                                                    {  rgb[x - 1, y + 1],  rgb[x, y + 1],  rgb[x + 1, y + 1], rgb[x+2, y + 1] },
                                                    {  rgb[x - 1, y + 2],  rgb[x, y + 2],  rgb[x + 1, y + 2], rgb[x+2, y + 2] },};
                    newPixel = Convert.ToInt32(bicubicInterpolate(p, 0.25, 0.5));
                    if (newPixel < 0) newPixel = 0;
                    if (newPixel > 255) newPixel = 255;
                    newColor = Color.FromArgb(newPixel, newPixel, newPixel);
                    image.SetPixel(x - 1, y * 2, newColor);
                }
            }
            pictureBox1.Image = image;

            //double[,] p = new double[4, 4] { { 1, 3, 3, 4 }, { 7, 2, 3, 4 }, { 1, 6, 3, 6 }, { 2, 5, 7, 2 } };
            //double mes = bicubicInterpolate(p, 0.1, 0.2);
            //MessageBox.Show(Convert.ToString(mes));
        }

        int BilinScal(int oldX, int oldY, int quantity, int num, Boolean hor = true) //координаты опорного пикселя, количество новых пикселей, между двумя базовыми, номер нужного пикселя по счёту, из заданного количества, горизонталь/вертикаль)
        {
            //int biba = 0;
            int newPix = 0;
            if (hor)
            {
                int val1 = rgb[oldX, oldY];
                int val2 = rgb[oldX + 1, oldY];
                newPix = (val2 - val1) * (oldX + num);
                newPix /= (quantity + 1 + oldX);
                newPix += val1;
            }
            else
            {
                int val1 = rgb[oldX, oldY];
                int val2 = rgb[oldX, oldY + 1];
                newPix = (val2 - val1) * (oldY + num);
                newPix /= (quantity + 1 + oldY);
                newPix += val1;
            }
            if (newPix < 0) newPix = 0;
            if (newPix > 255) newPix = 255;
            return newPix;
        }

        /*int BicubScal(int oldX, int oldY) //Внимание! Скалируем до краёв, их не задевая!
        {
            int newPix = 0;
            newPix = rgb[oldX, oldY] + rgb[oldX + 1, oldY] + rgb[oldX, oldY + 1] + rgb[oldX + 1, oldY + 1];
            newPix /= 4;
            if (newPix < 0) newPix = 0;
            if (newPix > 255) newPix = 255;
            return newPix;
        }*/
        double bicubicInterpolate(double[,] p, double x, double y)
        {
            double[] arr = new double[4];
            arr[0] = cubicInterpolate(p, y, 0);
            arr[1] = cubicInterpolate(p, y, 1);
            arr[2] = cubicInterpolate(p, y, 2);
            arr[3] = cubicInterpolate(p, y, 3);
            return cubicInterpolate(arr, x);
        }
        double cubicInterpolate(double[] p, double x)
        {
            return p[1] + 0.5 * x * (p[2] - p[0] + x * (2.0 * p[0] - 5.0 * p[1] + 4.0 * p[2] - p[3] + x * (3.0 * (p[1] - p[2]) + p[3] - p[0])));
        }
        double cubicInterpolate(double[,] p, double x, int y)
        {
            return p[y, 1] + 0.5 * x * (p[y, 2] - p[y, 0] + x * (2.0 * p[y, 0] - 5.0 * p[y, 1] + 4.0 * p[y, 2] - p[y, 3] + x * (3.0 * (p[y, 1] - p[y, 2]) + p[y, 3] - p[y, 0])));
        }
    }
}
